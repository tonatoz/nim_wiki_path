### WTF? ###

Program search terms chain between begin and end term at wikipedia (ru.wikipedia.org).

### How to use? ###

Arguments:
* <begining term>
* <ending term>

### Example ###

```
#!Nim

> ./wiki_path Nim SAP

Found! /wiki/SAP
parent: /wiki/Хронология_языков_программирования
parent: /wiki/Nim
```

### Compiling ###
```
#!Nim
> nim c -d:ssl wiki_path.nim
```