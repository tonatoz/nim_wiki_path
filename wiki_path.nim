import httpclient, streams, htmlparser,
    xmltree, strtabs, strutils,
    queues, cgi, os

proc printTerm(term: string) = 
    echo decodeUrl(term)

proc findLinks(url: string) : seq[string] =
    result = @[]
    let html = getContent(r"https://ru.wikipedia.org" & url)
    for a in parseHtml(newStringStream(html)).findAll("a"):
        let href = a.attrs["href"]
        if href.startsWith("/wiki/") and not href.contains({':', '#'}):
            result.add(href)

proc printParent(term: string, dict: StringTableRef) =
    printTerm("\nFound! " & term)
    var n = dict[term]
    while n != "#stop#":
        printTerm("parent: " & n)
        n = dict[n]

proc searchPath(beginTerm: string, endTerm: string) = 
    var dict = newStringTable(modeCaseInsensitive)
    var queue = initQueue[string]()

    dict[beginTerm] = "#stop#"
    queue.add(beginTerm)

    while true:
        var currentNode = queue.dequeue  
        printTerm(currentNode)
        for a in findLinks(currentNode):
            if not dict.hasKey(a):
                dict[a] = currentNode
                if a == endTerm:
                    printParent(a, dict)
                    return
                queue.add(a)

let args = commandLineParams()
case args.len:
    of 2:
        searchPath("/wiki/" & encodeUrl(args[0]), "/wiki/" & encodeUrl(args[1]))
    of 0:
        searchPath("/wiki/Sort", "/wiki/SAP")
    else:   
        echo r"use >>program <begin term> <end term>"